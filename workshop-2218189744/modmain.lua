local _G = GLOBAL

if _G.TheNet:GetIsClient() then return end

local admin = GetModConfigData("admin") or true
local radius_deploy = GetModConfigData("radius_deploy") or 24
local radius_read = GetModConfigData("radius_read") or 16
local radius_skeleton = GetModConfigData("radius_skeleton") or 6
local radius_lunarthrall_plant = GetModConfigData("radius_lunarthrall_plant") or 32

if radius_deploy ~= 0 then
    local original_DEPLOY = _G.ACTIONS.DEPLOY.fn
    _G.ACTIONS.DEPLOY.fn = function(act)
        if act.invobject ~= nil and
          (act.invobject.prefab == "spidereggsack" or
           act.invobject.prefab == "lureplantbulb" or
           act.invobject.prefab == "fossil_piece"  or
           --support "Shipwrecked Monsters R2" mod
           act.invobject.prefab == "dug_elephantcactus") then
            if admin and act.doer.Network:IsServerAdmin() then return original_DEPLOY(act) end

            local pos = act:GetActionPoint()
            local x, y, z = pos:Get()
            local ents = _G.TheSim:FindEntities(x, y, z, radius_deploy, {"multiplayer_portal"})
            if #ents > 0 then
                return false
            end
        end
        return original_DEPLOY(act)
    end
end

if radius_read ~= 0 then
    local original_READ = _G.ACTIONS.READ.fn
    _G.ACTIONS.READ.fn = function(act)
        if act.invobject ~= nil and act.invobject.prefab == "book_tentacles" then
            if admin and act.doer.Network:IsServerAdmin() then return original_READ(act) end

            local x, y, z = act.doer.Transform:GetWorldPosition()
            local ents = _G.TheSim:FindEntities(x, y, z, radius_read, {"multiplayer_portal"})
            if #ents > 0 then
                return false
            end
        end
        return original_READ(act)
    end
end

if radius_skeleton ~= 0 then
    AddPrefabPostInit("skeleton_player", function(inst)
        inst:DoTaskInTime(3, function (inst)
            local x, y, z = inst.Transform:GetWorldPosition()
            local ents = _G.TheSim:FindEntities(x, y, z, radius_skeleton, {"multiplayer_portal"})
            if #ents > 0 then
                _G.SpawnAt("small_puff", _G.Vector3(x, y, z))
                inst:Remove()
            end
        end)
    end)
end

if radius_lunarthrall_plant ~= 0 then
    AddPrefabPostInit("lunarthrall_plant", function(inst)
        inst:DoTaskInTime(3, function (inst)
            local x, y, z = inst.Transform:GetWorldPosition()
            local ents = _G.TheSim:FindEntities(x, y, z, radius_lunarthrall_plant, {"multiplayer_portal"})
            if #ents > 0 then
                _G.SpawnAt("small_puff", _G.Vector3(x, y, z))
                inst:Remove()
            end
        end)
    end)
end
