name = "Portal protected"
description = [[Protects the portal from reading the tentaclea book, planting a spider cocoon, a fossils, a lureplant next to it, as well as destroying the skeletons of players.

In the settings, you can change the radiuses or disable any protection. By default, the protection does not apply to admins, but you can enable it in the settings.

Recommended for public servers.]]
author = "surg"
version = "1.0.4"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 0
dont_starve_compatible = false
reign_of_giants_compatible = false
shipwrecked_compatible = false
hamlet_compatible = false
dst_compatible = true
all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {}

local radius_deploy = {}
local radius_read = {}
local radius_skeleton = {}
local radius_lunarthrall_plant = {}

for i = 1, 51 do
    local value = i - 1
    radius_deploy[i] = { description = ""..value, data = value }
    radius_read[i] = { description = ""..value, data = value }
    radius_skeleton[i] = { description = ""..value, data = value }
    radius_lunarthrall_plant[i] = { description = ""..value, data = value }
end

configuration_options =
{
    {
        name = "admin",
        label = "Enable admins",
        hover = "Admins can plant and read book tentacles.",
        options =
        {
            {description = "on", data = true, hover = "enable"},
            {description = "off", data = false, hover = "disabled"},
        },
        default = true,
    },
    {
        name    = "radius_deploy",
        label   = "Radius protects deploy",
        hover   = "Set the radius at which it is forbidden to plant.",
        options = radius_deploy,
        default = 24
    },
    {
        name    = "radius_read",
        label   = "Radius protects read",
        hover   = "Set the radius at which it is forbidden to read book tentacles.",
        options = radius_read,
        default = 16
    },
    {
        name    = "radius_skeleton",
        label   = "Radius destroy skeletons",
        hover   = "Set the radius at which is destroy player skeletons.",
        options = radius_skeleton,
        default = 6
    },
    {
        name    = "radius_lunarthrall_plant",
        label   = "Radius destroy deadly brightshade",
        hover   = "Set the radius at which is destroy deadly brightshade.",
        options = radius_lunarthrall_plant,
        default = 32
    },
}
